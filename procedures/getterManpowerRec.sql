CREATE DEFINER=`root`@`localhost` PROCEDURE `getterManpowerRecovery`(
	IN provincesID INT,
    OUT manRecovery DOUBLE)
BEGIN
	SELECT SUM((1+mEff.value)*(1-provinces.autonomy)*(provinceToStat.value)) INTO manRecovery FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
			JOIN product ON product.productID =provinces.product_productID
				JOIN 
					(SELECT provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="M" and property="Manpower Efficiency") as mEff
						ON mEff.provincesID = provinces.provincesID
				where category="M" and property="base" 
                and provinces.provincesID=provinceID
                group by provinces.name; 
END