CREATE DEFINER=`root`@`localhost` PROCEDURE `tradeGoodCountOfTradeNode`(IN node VARCHAR(45))
BEGIN
	SELECT product.name,count(*)
		FROM tradeNodes JOIN provinces ON tradeNodes.nodeID=provinces.tradeNodes_nodeID
			JOIN product ON product.productID=provinces.product_productID 
				where tradeNodes.node=node
					group by product.name;
END