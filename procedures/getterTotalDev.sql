CREATE DEFINER=`root`@`localhost` PROCEDURE `getterTotalDev`(
	IN provincesID INT,
    OUT totalDev INT)
BEGIN
	SELECT sum(provinceToStat.value) INTO totalDev FROM provinces 
		JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
			JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
					where property="base" and provinces.provincesID=provincesID
						group by provinces.provincesID;
END