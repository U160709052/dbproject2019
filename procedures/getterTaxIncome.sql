CREATE DEFINER=`root`@`localhost` PROCEDURE `getterTaxIncome`(
	IN provincesID INT,
    OUT taxIncome DOUBLE)
BEGIN
select sum((provinceToStat.value*mEff.value*(1-provinces.autonomy))) INTO taxIncome FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
			JOIN product ON product.productID =provinces.product_productID
				JOIN 
					(SELECT provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="T" and property="Income Efficiency") as mEff
						ON mEff.provincesID = provinces.provincesID
				where category="T" and property="base" 
                and provinces.provincesID=provincesID
				group by provinces.name;
END