CREATE DEFINER=`root`@`localhost` PROCEDURE `getterSupplyLimit`(
	IN provincesID INT,
    OUT supplyLimit DOUBLE)
BEGIN
	SELECT (provinceToStat.value*(1+supply.value)) INTO supplyLimit FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
				JOIN 
					(SELECT provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="M" and property="Supply Limit Efficiency") as supply
						ON supply.provincesID = provinces.provincesID
				where category="M" and property="base"
                and provinces.provincesID=provincesID; 
END