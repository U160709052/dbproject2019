CREATE DEFINER=`root`@`localhost` PROCEDURE `PercentageOfReligions`()
BEGIN
	SELECT religion.name,count(provinces.provincesID)*100/(Select Count(*) From provinces) as percentage FROM provinces 
		JOIN religion ON religion.religionID = provinces.religion_religionID
			group by religion.religionID
				order by percentage desc;
END