CREATE DEFINER=`root`@`localhost` PROCEDURE `getterProductionIncome`(
	IN provincesID INT,
    OUT pIncome DOUBLE)
BEGIN
	SELECT SUM((1+pEff.value)*(1-provinces.autonomy)*(provinceToStat.value)*(product.goodValue)) INTO pIncome FROM provinces 
		JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
			JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
				JOIN product ON product.productID =provinces.product_productID
					JOIN 
						(SELECT provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
							JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
								where category="P" and property="Production Efficiency") as pEff
							ON pEff.provincesID = provinces.provincesID
					where category="P" and property="base"
                    and provinces.provincesID=provincesID
					group by provinces.name; 

END