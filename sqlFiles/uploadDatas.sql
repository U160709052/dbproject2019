show variables like 'secure_file_priv';

/* /var/lib/mysql-files/csvFiles/fileName.csv */

/* ++Upload Continents++ */
load data local infile "/var/lib/mysql-files/csvFiles/continents.csv" into table continents fields terminated by ',';

select * from continents;
/* --Upload Continents-- */

/* ++Upload superRegions++ */
load data local infile "/var/lib/mysql-files/csvFiles/newSuperRegions.csv" into table superRegions fields terminated by ',';

select * from superRegions;
/* --Upload superRegions-- */

/* ++Upload regions++ */
load data local infile "/var/lib/mysql-files/csvFiles/newRegions.csv" into table regions fields terminated by ',';

select * from regions;
/* --Upload regions-- */

/* ++Upload areas++ */
load data local infile "/var/lib/mysql-files/csvFiles/newAreas.csv" into table areas fields terminated by ',';

select * from areas;
/* --Upload areas-- */

/* ++Upload geographicalMap++ */
load data local infile "/var/lib/mysql-files/csvFiles/geographicalMap.csv" into table geographicalMap fields terminated by ',';

select * from geographicalMap;
/* --Upload geographicalMap-- */

/* ++Upload cultures++ */
load data local infile "/var/lib/mysql-files/csvFiles/cultures.csv" into table cultures fields terminated by ',';

select * from cultures;
/* --Upload cultures-- */

/* ++Upload owners++ */
load data local infile "/var/lib/mysql-files/csvFiles/owners.csv" into table owners fields terminated by ',';

select * from owners;
/* --Upload owners-- */

/* ++Upload religion++ */
load data local infile "/var/lib/mysql-files/csvFiles/religion.csv" into table religion fields terminated by ',';

select * from religion;
/* --Upload religion-- */

/* ++Upload tradeNodes++ */
load data local infile "/var/lib/mysql-files/csvFiles/tradeNodes.csv" into table tradeNodes fields terminated by ',';

select * from tradeNodes;
/* --Upload tradeNodes-- */

/* ++Upload manufactories++ */
load data local infile "/var/lib/mysql-files/csvFiles/manufactories.csv" into table manufactories fields terminated by ',';

select * from manufactories;
/* --Upload manufactories-- */

/* ++Upload tradeCategory++ */
load data local infile "/var/lib/mysql-files/csvFiles/tradeCategory.csv" into table tradeCategory fields terminated by ',';

select * from tradeCategory;
/* --Upload tradeCategory-- */

/* ++Upload product++ */
load data local infile "/var/lib/mysql-files/csvFiles/product.csv" into table product fields terminated by ',';

select * from product;
/* --Upload product-- */

/* ++Upload statType++ for RDF */
load data local infile "/var/lib/mysql-files/csvFiles/statType.csv" into table statType fields terminated by ',';

select * from statType;
/* --Upload statType-- for RDF  */

/* ++Upload provinces++ */
load data local infile "/var/lib/mysql-files/csvFiles/type1Provinces.csv" into table provinces fields terminated by ',' (provincesID,name,estate,autonomy,unrest,maintance,fortLevel,HRE,MOH,geographicalMap_terrainID,cultures_cultureID,owners_ownerID,religion_religionID,product_productID,tradeNodes_nodeID,areas_areaID);

load data local infile "/var/lib/mysql-files/csvFiles/type2Provinces.csv" into table provinces fields terminated by ',' (provincesID,name,permanentModifier,estate,autonomy,unrest,maintance,fortLevel,HRE,MOH,geographicalMap_terrainID,cultures_cultureID,owners_ownerID,religion_religionID,product_productID,tradeNodes_nodeID,areas_areaID);

load data local infile "/var/lib/mysql-files/csvFiles/type3Provinces.csv" into table provinces fields terminated by ',' (provincesID,name,estate,autonomy,unrest,maintance,fortLevel,HRE,MOH,geographicalMap_terrainID,cultures_cultureID,religion_religionID,tradeNodes_nodeID,areas_areaID);

load data local infile "/var/lib/mysql-files/csvFiles/type4Provinces.csv" into table provinces fields terminated by ',' (provincesID,name,estate,autonomy,unrest,maintance,fortLevel,HRE,MOH,geographicalMap_terrainID,tradeNodes_nodeID,areas_areaID);

load data local infile "/var/lib/mysql-files/csvFiles/type5Provinces.csv" into table provinces fields terminated by ',' (provincesID,name,permanentModifier,estate,autonomy,unrest,maintance,fortLevel,HRE,MOH,geographicalMap_terrainID,cultures_cultureID,religion_religionID,tradeNodes_nodeID,areas_areaID);

load data local infile "/var/lib/mysql-files/csvFiles/type6Provinces.csv" into table provinces fields terminated by ',' (provincesID,name,estate,autonomy,unrest,maintance,fortLevel,HRE,MOH,geographicalMap_terrainID,cultures_cultureID,religion_religionID,product_productID,tradeNodes_nodeID,areas_areaID);

load data local infile "/var/lib/mysql-files/csvFiles/type7Provinces.csv" into table provinces fields terminated by ',' (provincesID,name,estate,autonomy,unrest,maintance,fortLevel,HRE,MOH,geographicalMap_terrainID,areas_areaID);

load data local infile "/var/lib/mysql-files/csvFiles/type8Provinces.csv" into table provinces fields terminated by ',' (provincesID,name,permanentModifier,estate,autonomy,unrest,maintance,fortLevel,HRE,MOH,geographicalMap_terrainID,cultures_cultureID,religion_religionID,product_productID,tradeNodes_nodeID,areas_areaID);

load data local infile "/var/lib/mysql-files/csvFiles/type9Provinces.csv" into table provinces fields terminated by ',' (provincesID,name,estate,autonomy,unrest,maintance,fortLevel,HRE,MOH,geographicalMap_terrainID,cultures_cultureID,owners_ownerID,religion_religionID,tradeNodes_nodeID,areas_areaID);

select * from provinces;
select count(*) from provinces;
/* --Upload provinces-- */

/* ++stat2Provinces FOR RDF ++ */
load data local infile "/var/lib/mysql-files/csvFiles/provinceToStat.csv" into table provinceToStat fields terminated by ',';

select * from provinceToStat;
/* -- stat2Provinces FOR RDF -- */

