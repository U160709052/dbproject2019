/* ++ Which area has the largest tax income ++ */
select areas.name, sum((baseTax.base*incomeEff.eff*(1-baseTax.autonomy))) as TotalTaxIncome from 
(
SELECT provinces.provincesID as id,provinces.name as name ,provinceToStat.value/12 as base,provinces.autonomy as autonomy FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
			where category="T" and property="base") as baseTax 

join 

(SELECT provinces.provincesID as id,provinceToStat.value as eff,provinces.areas_areaID as areaId FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
			where category="T" and property="Income Efficiency") as incomeEff
on incomeEff.id=baseTax.id

join
areas on incomeEff.areaId=areas.areaID
group by areas.areaID
order by TotalTaxIncome desc limit 1;

/* -- Which area has the largest tax income -- */

/* ++ What are the avarage of areas' development ++ */

SELECT areas.name,avg(provinceToStat.value) as totalDev FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
			JOIN areas on provinces.areas_areaID=areas.areaID
			where property="base"
            group by provinces.areas_areaID
            ORDER BY totalDev DESC;

/* -- What are the avarage of areas' development -- */

/* ++ What is the largest culture in terms of development ++ */

SELECT cultures.name,sum(provinceToStat.value) as totalDev FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
			JOIN cultures on provinces.cultures_cultureID=cultures.cultureID
			where property="base"
            group by provinces.cultures_cultureID
            ORDER BY totalDev DESC;

/* -- What is the largest culture in terms of development -- */

/* ++ What is the percentage of all religion in the world ++ */

SELECT religion.name,count(provinces.provincesID)*100/(Select Count(*) From provinces) as percentage FROM provinces 
	JOIN religion ON religion.religionID = provinces.religion_religionID
    group by religion.religionID
    order by percentage desc;

/* -- What is the percentage of all religion in the world -- */

/* ++ What are the total values of the products produced in the world ++ */

SELECT product.name,SUM((1+pEff.value)*(1-provinces.autonomy)*(provinceToStat.value)*(product.goodValue)) AS TotalProduced FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
			JOIN product ON product.productID =provinces.product_productID
				JOIN 
					(SELECT provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="P" and property="Production Efficiency") as pEff
						ON pEff.provincesID = provinces.provincesID
				where category="P" and property="base"
                group by product.name
                order by TotalProduced desc; 

/* -- What are the total values of the products produced in the world -- */

/* ++ Which province has to most manpower recovery for month ++ */

SELECT provinces.name,SUM((1+mEff.value)*(1-provinces.autonomy)*(provinceToStat.value)) AS Manpower FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
			JOIN product ON product.productID =provinces.product_productID
				JOIN 
					(SELECT provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="M" and property="Manpower Efficiency") as mEff
						ON mEff.provincesID = provinces.provincesID
				where category="M" and property="base"
                group by provinces.name
                order by Manpower desc; 

/* -- Which province has to most manpower recovery for month -- */

/* ++ What are the most valuable provinces by type of production income ++ */

SELECT provinces.name,SUM((1+pEff.value)*(1-provinces.autonomy)*(provinceToStat.value)*(product.goodValue)) AS TotalProduced FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
			JOIN product ON product.productID =provinces.product_productID
				JOIN 
					(SELECT provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="P" and property="Production Efficiency") as pEff
						ON pEff.provincesID = provinces.provincesID
				where category="P" and property="base"
                group by provinces.name
                order by TotalProduced desc; 

/* -- What are the most valuable provinces by type of production income -- */

/* ++ What is the percentage of the world is in HRE or MOH ++ */

SELECT * FROM 
	(SELECT count(provinces.HRE)*100/(Select Count(*) From provinces) AS HRE FROM provinces where HRE=TRUE group by HRE) as HRE,
	(SELECT count(provinces.HRE)*100/(Select Count(*) From provinces) AS MOH FROM provinces where MOH=TRUE group by MOH) as MOH;

/* -- What is the percentage of the world is in HRE or MOH -- */

/* ++ How many is the suply limit of all provinces ++ */

SELECT provinces.name,(provinceToStat.value*(1+supply.value)) AS SupplyLimit FROM provinces 
	JOIN provinceToStat ON provinceToStat.provincesID = provinces.provincesID
		JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
				JOIN 
					(SELECT provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						JOIN statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="M" and property="Supply Limit Efficiency") as supply
						ON supply.provincesID = provinces.provincesID
				where category="M" and property="base"
                order by SupplyLimit desc; 

/* -- How many is the suply limit of all provinces -- */

/* ++ What is the total number of regions of the continents ++ */

select continents.name,count(regions.region) from regions join superRegions on regions.superRegions_sprRegionID=superRegions.sprRegionID
	join continents on superRegions.continents_continentID=continents.continentID
    group by continents.name;
    
/* -- What is the total number of regions of the continents -- */