-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `euIVdb` DEFAULT CHARACTER SET utf8 ;
USE `euIVdb` ;
-- -----------------------------------------------------
-- Table `geographicalMap`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `geographicalMap` (
  `terrainID` INT NOT NULL, Sample Data= 1
  `type` VARCHAR(45) NOT NULL, Sample Data= "Land"
  `terrain` VARCHAR(45) NOT NULL, Sample Data= "Grassland"
  PRIMARY KEY (`terrainID`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `cultures`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cultures` (
  `cultureID` INT NOT NULL, Sample Data= 1
  `name` VARCHAR(100) NOT NULL, Sample Data= "Swedish"
  PRIMARY KEY (`cultureID`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `owners`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `owners` (
  `ownerID` INT NOT NULL, Sample Data= 1
  `name` VARCHAR(45) NOT NULL, Sample Data= "Sweden"
  PRIMARY KEY (`ownerID`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `religion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `religion` (
  `religionID` INT NOT NULL, Sample Data= 1
  `name` VARCHAR(45) NOT NULL, Sample Data= "Catholic"
  PRIMARY KEY (`religionID`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `manufactories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `manufactories` (
  `manufactoryID` INT NOT NULL,Sample Data= 1
  `name` VARCHAR(45) NOT NULL,Sample Data= "Farm Estate"
  PRIMARY KEY (`manufactoryID`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `tradeCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tradeCategory` (
  `categoryID` INT NOT NULL,Sample Data= 1
  `category` VARCHAR(80) NOT NULL,Sample Data= "Base"
  PRIMARY KEY (`categoryID`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `product` (
  `productID` INT NOT NULL,Sample Data= 1
  `name` VARCHAR(45) NOT NULL,Sample Data= "Chinaware"
  `goodValue` FLOAT NOT NULL,Sample Data= 3.0
  `manufactories_manufactoryID` INT NOT NULL,Sample Data= 6 (from manufactories=Mill)
  `tradeCategory_categoryID` INT NOT NULL,Sample Data= 6 (from tradeCategory=Eastern)
  PRIMARY KEY (`productID`),
  INDEX `fk_product_manufactories1_idx` (`manufactories_manufactoryID` ASC),
  INDEX `fk_product_tradeCategory1_idx` (`tradeCategory_categoryID` ASC),
  CONSTRAINT `fk_product_manufactories1`
    FOREIGN KEY (`manufactories_manufactoryID`)
    REFERENCES `manufactories` (`manufactoryID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_tradeCategory1`
    FOREIGN KEY (`tradeCategory_categoryID`)
    REFERENCES `tradeCategory` (`categoryID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION)
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `tradeNodes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tradeNodes` (
  `nodeID` INT NOT NULL,Sample Data= 1
  `node` VARCHAR(45) NOT NULL,Sample Data= "Baltic Sea"
  PRIMARY KEY (`nodeID`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `continents`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `continents` (
  `continentID` INT NOT NULL,Sample Data= 1
  `name` VARCHAR(45) NOT NULL,Sample Data= Europe
  PRIMARY KEY (`continentID`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `superRegions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `superRegions` (
  `sprRegionID` INT NOT NULL,Sample Data= 1
  `name` VARCHAR(45) NOT NULL,Sample Data= "Western Europe"
  `continents_continentID` INT NOT NULL,Sample Data= 1 (from continents=Europe)
  PRIMARY KEY (`sprRegionID`),
  INDEX `fk_superRegions_continents1_idx` (`continents_continentID` ASC),
  CONSTRAINT `fk_superRegions_continents1`
    FOREIGN KEY (`continents_continentID`)
    REFERENCES `continents` (`continentID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `regions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `regions` (
  `regionID` INT NOT NULL,Sample Data= 1
  `region` VARCHAR(60) NOT NULL,Sample Data= "Scandinavia"
  `superRegions_sprRegionID` INT NOT NULL,Sample Data= 1 (from superRegion=Western Europe)
  PRIMARY KEY (`regionID`),
  INDEX `fk_regions_superRegions1_idx` (`superRegions_sprRegionID` ASC),
  CONSTRAINT `fk_regions_superRegions1`
    FOREIGN KEY (`superRegions_sprRegionID`)
    REFERENCES `superRegions` (`sprRegionID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `areas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `areas` (
  `areaID` INT NOT NULL,Sample Data= 1
  `name` VARCHAR(60) NOT NULL,Sample Data="Östra Svealand"
  `regions_regionID` INT NOT NULL,Sample Data= 1 (from regions=Scandinavia)
  PRIMARY KEY (`areaID`),
  INDEX `fk_areas_regions1_idx` (`regions_regionID` ASC),
  CONSTRAINT `fk_areas_regions1`
    FOREIGN KEY (`regions_regionID`)
    REFERENCES `regions` (`regionID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `provinces`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `provinces` (
  `provincesID` INT NOT NULL,Sample Data= 1
  `name` VARCHAR(125) NOT NULL,Sample Data= "Stockholm"
  `permanentModifier` VARCHAR(80) NULL,Sample Data= Null
  `estate` VARCHAR(20) NULL,Sample Data= "No Estate"
  `autonomy` FLOAT NULL,Sample Data= 0.0
  `unrest` FLOAT NULL,Sample Data= 0
  `maintance` FLOAT NULL,Sample Data= 0.04
  `fortLevel` INT NULL,Sample Data= 1
  `HRE` TINYINT(1) NULL,Sample Data= 0
  `MOH` TINYINT(1) NULL,Sample Data= 0
  `geographicalMap_terrainID` INT NULL,Sample Data= 1 (from geographicalMap=Grassland)
  `cultures_cultureID` INT NULL,Sample Data= 1 (from cultures=Swedish)
  `owners_ownerID` INT NULL,Sample Data= 1 (from owners=Sweden)
  `religion_religionID` INT NULL,Sample Data= 1 (from religion=Catholic)
  `product_productID` INT NULL,Sample Data= 13 (from product=Grain)
  `tradeNodes_nodeID` INT NULL,Sample Data= 1 (from tradeNodes=Baltic Sea)
  `areas_areaID` INT NOT NULL,Sample Data= 1 (from areas=Östra Svealand)
  PRIMARY KEY (`provincesID`),
  INDEX `fk_provinces_geographicalMap_idx` (`geographicalMap_terrainID` ASC),
  INDEX `fk_provinces_cultures1_idx` (`cultures_cultureID` ASC),
  INDEX `fk_provinces_owners1_idx` (`owners_ownerID` ASC),
  INDEX `fk_provinces_religion1_idx` (`religion_religionID` ASC),
  INDEX `fk_provinces_product1_idx` (`product_productID` ASC),
  INDEX `fk_provinces_tradeNodes1_idx` (`tradeNodes_nodeID` ASC),
  INDEX `fk_provinces_areas1_idx` (`areas_areaID` ASC),
  CONSTRAINT `fk_provinces_geographicalMap`
    FOREIGN KEY (`geographicalMap_terrainID`)
    REFERENCES `geographicalMap` (`terrainID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_cultures1`
    FOREIGN KEY (`cultures_cultureID`)
    REFERENCES `cultures` (`cultureID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_owners1`
    FOREIGN KEY (`owners_ownerID`)
    REFERENCES `owners` (`ownerID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_religion1`
    FOREIGN KEY (`religion_religionID`)
    REFERENCES `religion` (`religionID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_product1`
    FOREIGN KEY (`product_productID`)
    REFERENCES `product` (`productID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_tradeNodes1`
    FOREIGN KEY (`tradeNodes_nodeID`)
    REFERENCES `tradeNodes` (`nodeID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_areas1`
    FOREIGN KEY (`areas_areaID`)
    REFERENCES `areas` (`areaID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION)
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `statType`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `statType` (
  `statTypeID` INT NOT NULL,Sample Data= 1 
  `category` CHAR(1) NOT NULL,Sample Data= 'T'
  `property` VARCHAR(45) NOT NULL,Sample Data= "Base" 
  PRIMARY KEY (`statTypeID`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `provinceToStat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `provinceToStat` (
  `provincesID` INT NOT NULL,Sample Data= 1 (from provinces=Stockholm)
  `statTypeID` INT NOT NULL,Sample Data= 1 (from statType=T-BASE)
  `value` DOUBLE NOT NULL,Sample Data= 5
  PRIMARY KEY (`provincesID`, `statTypeID`),
  INDEX `fk_provinces_has_statType_statType1_idx` (`statTypeID` ASC),
  INDEX `fk_provinces_has_statType_provinces1_idx` (`provincesID` ASC),
  CONSTRAINT `fk_provinces_has_statType_provinces1`
    FOREIGN KEY (`provincesID`)
    REFERENCES `provinces` (`provincesID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_has_statType_statType1`
    FOREIGN KEY (`statTypeID`)
    REFERENCES `statType` (`statTypeID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION)
ENGINE = InnoDB;

