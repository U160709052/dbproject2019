-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `euIVdb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `euIVdb` DEFAULT CHARACTER SET utf8 ;
USE `euIVdb` ;

-- -----------------------------------------------------
-- Table `geographicalMap`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `geographicalMap` ;

CREATE TABLE IF NOT EXISTS `geographicalMap` (
  `terrainID` INT NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `terrain` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`terrainID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cultures`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cultures` ;

CREATE TABLE IF NOT EXISTS `cultures` (
  `cultureID` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`cultureID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `owners`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `owners` ;

CREATE TABLE IF NOT EXISTS `owners` (
  `ownerID` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ownerID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `religion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `religion` ;

CREATE TABLE IF NOT EXISTS `religion` (
  `religionID` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`religionID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `manufactories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `manufactories` ;

CREATE TABLE IF NOT EXISTS `manufactories` (
  `manufactoryID` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`manufactoryID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tradeCategory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tradeCategory` ;

CREATE TABLE IF NOT EXISTS `tradeCategory` (
  `categoryID` INT NOT NULL,
  `category` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`categoryID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `product` ;

CREATE TABLE IF NOT EXISTS `product` (
  `productID` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `goodValue` FLOAT NOT NULL,
  `manufactories_manufactoryID` INT NOT NULL,
  `tradeCategory_categoryID` INT NOT NULL,
  PRIMARY KEY (`productID`),
  INDEX `fk_product_manufactories1_idx` (`manufactories_manufactoryID` ASC),
  INDEX `fk_product_tradeCategory1_idx` (`tradeCategory_categoryID` ASC),
  CONSTRAINT `fk_product_manufactories1`
    FOREIGN KEY (`manufactories_manufactoryID`)
    REFERENCES `manufactories` (`manufactoryID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_tradeCategory1`
    FOREIGN KEY (`tradeCategory_categoryID`)
    REFERENCES `tradeCategory` (`categoryID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tradeNodes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tradeNodes` ;

CREATE TABLE IF NOT EXISTS `tradeNodes` (
  `nodeID` INT NOT NULL,
  `node` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`nodeID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `continents`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `continents` ;

CREATE TABLE IF NOT EXISTS `continents` (
  `continentID` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`continentID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `superRegions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `superRegions` ;

CREATE TABLE IF NOT EXISTS `superRegions` (
  `sprRegionID` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `continents_continentID` INT NOT NULL,
  PRIMARY KEY (`sprRegionID`),
  INDEX `fk_superRegions_continents1_idx` (`continents_continentID` ASC),
  CONSTRAINT `fk_superRegions_continents1`
    FOREIGN KEY (`continents_continentID`)
    REFERENCES `continents` (`continentID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `regions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `regions` ;

CREATE TABLE IF NOT EXISTS `regions` (
  `regionID` INT NOT NULL,
  `region` VARCHAR(60) NOT NULL,
  `superRegions_sprRegionID` INT NOT NULL,
  PRIMARY KEY (`regionID`),
  INDEX `fk_regions_superRegions1_idx` (`superRegions_sprRegionID` ASC),
  CONSTRAINT `fk_regions_superRegions1`
    FOREIGN KEY (`superRegions_sprRegionID`)
    REFERENCES `superRegions` (`sprRegionID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `areas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `areas` ;

CREATE TABLE IF NOT EXISTS `areas` (
  `areaID` INT NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  `regions_regionID` INT NOT NULL,
  PRIMARY KEY (`areaID`),
  INDEX `fk_areas_regions1_idx` (`regions_regionID` ASC),
  CONSTRAINT `fk_areas_regions1`
    FOREIGN KEY (`regions_regionID`)
    REFERENCES `regions` (`regionID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `provinces`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `provinces` ;

CREATE TABLE IF NOT EXISTS `provinces` (
  `provincesID` INT NOT NULL,
  `name` VARCHAR(125) NOT NULL,
  `permanentModifier` VARCHAR(80) NULL,
  `estate` VARCHAR(20) NULL,
  `autonomy` FLOAT NULL,
  `unrest` FLOAT NULL,
  `maintance` FLOAT NULL,
  `fortLevel` INT NULL,
  `HRE` TINYINT(1) NULL,
  `MOH` TINYINT(1) NULL,
  `geographicalMap_terrainID` INT NULL,
  `cultures_cultureID` INT NULL,
  `owners_ownerID` INT NULL,
  `religion_religionID` INT NULL,
  `product_productID` INT NULL,
  `tradeNodes_nodeID` INT NULL,
  `areas_areaID` INT NOT NULL,
  PRIMARY KEY (`provincesID`),
  INDEX `fk_provinces_geographicalMap_idx` (`geographicalMap_terrainID` ASC),
  INDEX `fk_provinces_cultures1_idx` (`cultures_cultureID` ASC),
  INDEX `fk_provinces_owners1_idx` (`owners_ownerID` ASC),
  INDEX `fk_provinces_religion1_idx` (`religion_religionID` ASC),
  INDEX `fk_provinces_product1_idx` (`product_productID` ASC),
  INDEX `fk_provinces_tradeNodes1_idx` (`tradeNodes_nodeID` ASC),
  INDEX `fk_provinces_areas1_idx` (`areas_areaID` ASC),
  CONSTRAINT `fk_provinces_geographicalMap`
    FOREIGN KEY (`geographicalMap_terrainID`)
    REFERENCES `geographicalMap` (`terrainID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_cultures1`
    FOREIGN KEY (`cultures_cultureID`)
    REFERENCES `cultures` (`cultureID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_owners1`
    FOREIGN KEY (`owners_ownerID`)
    REFERENCES `owners` (`ownerID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_religion1`
    FOREIGN KEY (`religion_religionID`)
    REFERENCES `religion` (`religionID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_product1`
    FOREIGN KEY (`product_productID`)
    REFERENCES `product` (`productID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_tradeNodes1`
    FOREIGN KEY (`tradeNodes_nodeID`)
    REFERENCES `tradeNodes` (`nodeID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_areas1`
    FOREIGN KEY (`areas_areaID`)
    REFERENCES `areas` (`areaID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `statType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `statType` ;

CREATE TABLE IF NOT EXISTS `statType` (
  `statTypeID` INT NOT NULL,
  `category` CHAR(1) NOT NULL,
  `property` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`statTypeID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `provinceToStat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `provinceToStat` ;

CREATE TABLE IF NOT EXISTS `provinceToStat` (
  `provincesID` INT NOT NULL,
  `statTypeID` INT NOT NULL,
  `value` DOUBLE NOT NULL,
  PRIMARY KEY (`provincesID`, `statTypeID`),
  INDEX `fk_provinces_has_statType_statType1_idx` (`statTypeID` ASC),
  INDEX `fk_provinces_has_statType_provinces1_idx` (`provincesID` ASC),
  CONSTRAINT `fk_provinces_has_statType_provinces1`
    FOREIGN KEY (`provincesID`)
    REFERENCES `provinces` (`provincesID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_provinces_has_statType_statType1`
    FOREIGN KEY (`statTypeID`)
    REFERENCES `statType` (`statTypeID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
