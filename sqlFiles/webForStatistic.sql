 /* geographicalMap */
select geographicalmap.type, geographicalmap.terrain, count(provinces.name) as totalProvince from provinces
	join geographicalMap on geographicalMap.terrainID = provinces.geographicalMap_terrainID
		group by geographicalmap.type,geographicalmap.terrain ;

 
    /* politcalMap */
    /* continen*/
select geoPoliticMap.Continent as Continent , count(distinct geoPoliticMap.SuperRegion) as TotalSuperRegion , count(distinct geoPoliticMap.region) as TotalRegion , count(distinct geoPoliticMap.area) as TotalArea , count(distinct geoPoliticMap.ProvinceName) as TotalProvince, sum(provinceDev.development) as TotalDev from  geoPoliticMap
    left join
(select sum(provinceToStat.value) as development , provinces.provincesID as ID  from provinces
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on statType.statTypeID = provinceToStat.statTypeID
				where property = "base" 
                group by provinces.provincesID) as provinceDev on provinceDev.ID = geoPoliticMap.ProvinceID 
                group by geoPoliticMap.Continent ;
    /*superregion*/
select geoPoliticMap.Continent as Continent,  geoPoliticMap.SuperRegion  as SuperRegion , count(distinct geoPoliticMap.region) as TotalRegion , count(distinct geoPoliticMap.area) as TotalArea , count(distinct geoPoliticMap.ProvinceName) as TotalProvince, sum(provinceDev.development) as TotalDev from  geoPoliticMap
    left join
(select sum(provinceToStat.value) as development , provinces.provincesID as ID  from provinces
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on statType.statTypeID = provinceToStat.statTypeID
				where property = "base" 
                group by provinces.provincesID) as provinceDev on provinceDev.ID = geoPoliticMap.ProvinceID 
                group by geoPoliticMap.SuperRegion,geoPoliticMap.Continent ;
/* region */
select geoPoliticMap.Continent as Continent,  geoPoliticMap.SuperRegion  as SuperRegion,  geoPoliticMap.region  as Region, count(distinct geoPoliticMap.area) as TotalArea , count(distinct geoPoliticMap.ProvinceName) as TotalProvince, sum(provinceDev.development) as TotalDev from  geoPoliticMap
    left join
(select sum(provinceToStat.value) as development , provinces.provincesID as ID  from provinces
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on statType.statTypeID = provinceToStat.statTypeID
				where property = "base" 
                group by provinces.provincesID) as provinceDev on provinceDev.ID = geoPoliticMap.ProvinceID 
                group by geoPoliticMap.Region,geoPoliticMap.SuperRegion,geoPoliticMap.Continent ;
select geoPoliticMap.Continent as Continent,  geoPoliticMap.SuperRegion  as SuperRegion,  geoPoliticMap.region  as Region, geoPoliticMap.area AS Area ,count(distinct geoPoliticMap.ProvinceName) as TotalProvince, sum(provinceDev.development) as TotalDev from  geoPoliticMap
    left join
(select sum(provinceToStat.value) as development , provinces.provincesID as ID  from provinces
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on statType.statTypeID = provinceToStat.statTypeID
				where property = "base" 
                group by provinces.provincesID) as provinceDev on provinceDev.ID = geoPoliticMap.ProvinceID 
                group by geoPoliticMap.Area,geoPoliticMap.Region,geoPoliticMap.SuperRegion,geoPoliticMap.Continent ;
	

 /* cultures */ 	
 select cultures.name , floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev  from provinces
	join cultures on cultures.cultureID = provinces.cultures_cultureID
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on provinceToStat.statTypeID =statType.statTypeID
				where property="base" 
				group by cultures.name;
                
/* owners */ 
select owners.name , floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev , count(provinces.provincesID)*100/((Select Count(*) From provinces)*3) as percentage from provinces
	join owners on owners.ownerID = provinces.owners_ownerID
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on provinceToStat.statTypeID =statType.statTypeID
				where property="base" 
				group by owners.name;
/* religon */
select religion.name , floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev  from provinces
	join religion on religion.religionID = provinces.religion_religionID
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on provinceToStat.statTypeID =statType.statTypeID
				where property="base" 
				group by religion.name;
/* tradeNode*/
select tradenodes.node , floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev , sum((1+pEff.value)*(1-provinces.autonomy)*(baseP.value)*(product.goodValue)) AS TotalProduced from provinces
	join tradenodes on tradenodes.nodeID = provinces.tradeNodes_nodeID
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on provinceToStat.statTypeID =statType.statTypeID
				join product ON product.productID =provinces.product_productID
					join 
					(select provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						join statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="P" and property="Production Efficiency") as pEff
                            on pEff.provincesID = provinces.provincesID
					join ( select provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
							join statType on provinceToStat.statTypeID =statType.statTypeID
							where category="P" and property="base") as baseP
							on baseP.provincesID = provinces.provincesID
					where property = "base"
				group by tradenodes.node;
                
/* product */
	select product.name, tradecategory.category, floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev , sum((1+pEff.value)*(1-provinces.autonomy)*(baseP.value)*(product.goodValue)) AS TotalProduced from provinces
		join product on product.productID = provinces.product_productID
			join tradecategory on tradecategory.categoryID = product.tradeCategory_categoryID
				join provinceToStat on provinceToStat.provincesID = provinces.provincesID
					join statType on provinceToStat.statTypeID =statType.statTypeID
                    join 
					(select provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						join statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="P" and property="Production Efficiency") as pEff
                            on pEff.provincesID = provinces.provincesID
					join ( select provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
							join statType on provinceToStat.statTypeID =statType.statTypeID
							where category="P" and property="base") as baseP
							on baseP.provincesID = provinces.provincesID
					where property = "base"
                    group by product.name;
                    
/*select sum((1+pEff.value)*(1-provinces.autonomy)*(provincetostat.value)*(product.goodValue)) AS TotalProduced2 from provinces
	join provincetostat on provincetostat.provincesID = provinces.provincesID
		join stattype on provincetostat.statTypeID = statType.statTypeID
			join product on product.productID = provinces.product_productID
			join 
					(select provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
						join statType ON provinceToStat.statTypeID =statType.statTypeID
							where category="P" and property="Production Efficiency") as pEff
                            on pEff.provincesID = provinces.provincesID
                            where category = "P" and property = "base"*/
/* membership */
		/*MOH*/
	select provinces.MOH , floor(count(provinces.name)/3) as totalProvince, sum(provinceToStat.value) as totalDev  from provinces
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on provinceToStat.statTypeID =statType.statTypeID
				where property = "base"
                group by provinces.MOH ;
		/*HRE*/
	select provinces.HRE , floor(count(provinces.name)/3) as totalProvince, sum(provinceToStat.value) as totalDev  from provinces
		join provinceToStat on provinceToStat.provincesID = provinces.provincesID
			join statType on provinceToStat.statTypeID =statType.statTypeID
				where property = "base"
                group by provinces.HRE ;
