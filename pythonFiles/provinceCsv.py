f = open("provinces.csv", "r")
provinces=[]
for line in f:
    line=line[:-1]
    lines=line.split(",")
    provinces.append(lines)
f.close()

# Terrains
f = open("geographicalMap.csv", "r")
terrains=[]
for line in f:
    line=line[:-1]
    lines=line.split(",")
    terrains.append(lines)
f.close()

# Cultures
f = open("cultures.csv", "r")
cultures=[]
for line in f:
    line=line[:-1]
    lines=line.split(",")
    cultures.append(lines)
f.close()

# Owners
f = open("owners.csv", "r")
owners=[]
for line in f:
    line=line[:-1]
    lines=line.split(",")
    owners.append(lines)
f.close()

# Religions
f = open("religion.csv", "r")
religions=[]
for line in f:
    line=line[:-1]
    lines=line.split(",")
    religions.append(lines)
f.close()

# Products
f = open("product.csv", "r")
products=[]
for line in f:
    line=line[:-1]
    lines=line.split(",")
    products.append(lines)
f.close()

# Trade Node
f = open("tradeNodes.csv", "r")
tradeNodes=[]
for line in f:
    line=line[:-1]
    lines=line.split(",")
    tradeNodes.append(lines)
f.close()

# Areas
f = open("newAreas.csv", "r")
areas=[]
for line in f:
    line=line[:-1]
    lines=line.split(",")
    areas.append(lines)
f.close()



for line in range(len(provinces)):
    print(provinces[line][10])
    for terrain in terrains:
        if terrain[2]==provinces[line][10]:
            provinces[line][10]=terrain[0]
            break


    print(provinces[line][11])
    for culture in cultures:
        if culture[1]==provinces[line][11]:
            provinces[line][11]=culture[0]
            break


    print(provinces[line][12])
    for owner in owners:
        if owner[1]==provinces[line][12]:
            provinces[line][12]=owner[0]
            break


    print(provinces[line][13])
    for religion in religions:
        if religion[1]==provinces[line][13]:
            provinces[line][13]=religion[0]
            break

    print(provinces[line][14])
    for product in products:
        if product[1]==provinces[line][14]:
            provinces[line][14]=product[0]
            break

    print(provinces[line][15])
    for trade in tradeNodes:
        if trade[1]==provinces[line][15]:
            provinces[line][15]=trade[0]
            break

    
    print(provinces[line][16])
    for area in areas:
        if area[1]==provinces[line][16]:
            provinces[line][16]=area[0]
            break

f = open("finalProvinces.csv", "w")
for line in provinces:
    for i in range(len(line)-1):
        f.write(line[i]+",")
    f.write(line[16])
    f.write("\n")