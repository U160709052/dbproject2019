CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `provinceForeignStat` AS
    SELECT 
        `provinces`.`provincesID` AS `ProvinceID`,
        `provinces`.`name` AS `ProvinceName`,
        `provinces`.`permanentModifier` AS `Modifier`,
        `provinces`.`estate` AS `Estate`,
        `provinces`.`autonomy` AS `Autonomy`,
        `provinces`.`unrest` AS `Unrest`,
        `provinces`.`maintance` AS `Maintance`,
        `provinces`.`fortLevel` AS `FortLevel`,
        `provinces`.`HRE` AS `HRE`,
        `provinces`.`MOH` AS `MOH`,
        `cultures`.`name` AS `Culture`,
        `manufactories`.`name` AS `Manufactory`,
        `owners`.`name` AS `Owner`,
        `religion`.`name` AS `Religion`,
        `product`.`name` AS `Product`,
        `tradeCategory`.`category` AS `TradeCategory`,
        `tradeNodes`.`node` AS `TradeNode`
    FROM
        (((((((`provinces`
        LEFT JOIN `cultures` ON ((`provinces`.`cultures_cultureID` = `cultures`.`cultureID`)))
        LEFT JOIN `owners` ON ((`provinces`.`owners_ownerID` = `owners`.`ownerID`)))
        LEFT JOIN `religion` ON ((`provinces`.`religion_religionID` = `religion`.`religionID`)))
        LEFT JOIN `tradeNodes` ON ((`provinces`.`tradeNodes_nodeID` = `tradeNodes`.`nodeID`)))
        LEFT JOIN `product` ON ((`provinces`.`product_productID` = `product`.`productID`)))
        LEFT JOIN `manufactories` ON ((`product`.`manufactories_manufactoryID` = `manufactories`.`manufactoryID`)))
        LEFT JOIN `tradeCategory` ON ((`product`.`tradeCategory_categoryID` = `tradeCategory`.`categoryID`)))