CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `geoPoliticMap` AS
    SELECT 
        `provinces`.`provincesID` AS `ProvinceID`,
        `provinces`.`name` AS `ProvinceName`,
        `continents`.`name` AS `Continent`,
        `superRegions`.`name` AS `SuperRegion`,
        `regions`.`region` AS `Region`,
        `areas`.`name` AS `Area`,
        `geographicalMap`.`type` AS `Type`,
        `geographicalMap`.`terrain` AS `Terrain`
    FROM
        (((((`provinces`
        JOIN `geographicalMap` ON ((`provinces`.`geographicalMap_terrainID` = `geographicalMap`.`terrainID`)))
        JOIN `areas` ON ((`provinces`.`areas_areaID` = `areas`.`areaID`)))
        JOIN `regions` ON ((`regions`.`regionID` = `areas`.`regions_regionID`)))
        JOIN `superRegions` ON ((`regions`.`superRegions_sprRegionID` = `superRegions`.`sprRegionID`)))
        JOIN `continents` ON ((`continents`.`continentID` = `superRegions`.`continents_continentID`)))