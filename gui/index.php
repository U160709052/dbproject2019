<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Europa Universalis 4 DB Project</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="dist/css/skins/skin-green.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green sidebar-mini">
<?php
    try {
        $myPDO = new PDO('mysql:host=localhost:3306;dbname=euIVdb;charset=utf8', 'root', '385914');
        //$link = mysqli_connect("localhost:3306", "root", "385914", "euIVdb");
    } 
    catch ( PDOException $e ){
        print $e->getMessage();
    }
?>
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="index.php?type=ap" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>EU</b>IV</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>EU-IV</b> DB</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">CATEGORIES OF DATABASE</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="index.php?type=ap"><i class="fa fa-database"> </i> <span>All Provinces</span></a></li>
        <li><a href="index.php?type=gm"><i class="fa fa-globe"></i> <span>Geographical Map</span></a></li>
        <li class="treeview">
          <a href="#"><i class="fa fa-map"></i> <span>Political Map</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?type=pmc">Continent</a></li>
            <li><a href="index.php?type=pmsr">Super Regions</a></li>
            <li><a href="index.php?type=pmr">Regions</a></li>
            <li><a href="index.php?type=pma">Areas</a></li>
          </ul>
        </li>
        <li><a href="index.php?type=c"><i class="fa fa-user"></i> <span>Cultures</span></a></li>
        <li><a href="index.php?type=o"><i class="fa fa-flag"></i> <span>Owners</span></a></li>
        <li><a href="index.php?type=r"><i class="fa fa-heart"></i></i> <span>Religions</span></a></li>
        <li><a href="index.php?type=tn"><i class="fa fa-dollar"></i> <span>Trade Nodes</span></a></li>
        <li><a href="index.php?type=p"><i class="fa fa-industry"></i> <span>Product</span></a></li>
        <li><a href="index.php?type=m"><i class="fa fa-empire"></i> <span>Memberships</span></a></li>
        <li><a href="charts.php"><i class="fa fa-pie-chart"></i> <span>Charts</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <?php 
      //Start Data Table

      //FOR PROVINCES
      if (!$_GET["type"] or $_GET["type"]=="ap") {
        $columsName=$myPDO->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'geoPoliticMap' or TABLE_NAME = 'provinceForeignStat'");
        $columns=array();
        $filter=array("ProvinceName","Culture","Owner","Religion","TradeNode","Product","Development","SuperRegion");
        $types=array("Land","Lake","InlandSea","Sea","Wasteland");
        $types2=array("Land","Lake","InlandSea","Sea","Wasteland");
        if(isset($_POST['filter'])) {
          $filter = $_POST['filter'];
        }
        if(isset($_POST['types'])) {
          $types = $_POST['types'];
        }
        if ( $columsName->rowCount() ){
          foreach( $columsName as $row ){
            array_push($columns,$row['COLUMN_NAME']);
          }
          array_push($columns,"Development");
          array_push($columns,"TaxIncome");
          array_push($columns,"ProductionIncome");
          array_push($columns,"ManpowerRecovery");
          array_push($columns,"SupplyLimit");
        }
    ?>

    <section class="content-header">
      <h1>
        Provinces
        <small>Optional Properties</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
    <section class ="filter" style="margin:15px;">
      <form action="index.php" method="post" class="form-inline">
      <i style="font-weight:bolder; margin-left:8px;">For Properties:</i>
        <?php 
          $added=array();
          foreach( $columns as $column ){
            if (!in_array($column, $added)) {
              array_push($added,$column);
            

        ?>
        <label class="checkbox-inline">
          <input type="checkbox" name="filter[]" id="inlineCheckbox" value=<?php print($column) ?> <?php if(in_array($column, $filter)) {print("checked");} ?>> <?php print($column) ?>
        </label>
        <?php 
            }
          }
        ?>
        <div>
          <i  style="font-weight:bolder; margin-left:8px;">For Types:</i>
        <?php 
          foreach( $types2 as $type ){
        ?>
          <label class="checkbox-inline">
            <input type="checkbox" name="types[]" id="inlineCheckbox2" value=<?php print($type) ?> <?php if(in_array($type, $types)) {print("checked");} ?>> <?php print($type) ?>
          </label>
        <?php 
          }
        ?>
        </div>
        <div>
        <button type="submit" class="btn btn-success">FILTER</button>
        </div>
      </form>
    </section>
    <!-- Main content -->
    <section class="content container-fluid">

        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      
                      <tr>
                        <?php 
                        foreach( $filter as $col ){
                          print("<th>$col</th>");
                        }
                        ?>
                      </tr>
                      </thead>
                      <tbody>
                      <?php  
                      // datas content
                      $where="where";
                      foreach ($types as $t) {
                        $where=$where." geoPoliticMap.Type='$t' or";
                      }
                      $where=substr($where,0,-2);
                        $query = $myPDO->query("SELECT * FROM geoPoliticMap JOIN provinceForeignStat ON geoPoliticMap.ProvinceID=provinceForeignStat.ProvinceID $where;");
                        if ( $query->rowCount() ){
                          foreach( $query as $row ){
                               
                      ?>
                      <tr>
                        <?php 
                          foreach( $filter as $col ){
                            if ($col=="Development" || $col=="TaxIncome" || $col=="ProductionIncome"|| $col=="ManpowerRecovery"|| $col=="SupplyLimit") {
                              // prepare for execution of the stored procedure
                              switch ($col) {
                                case "Development":
                                    $procedure="getterTotalDev";
                                    break;
                                case "TaxIncome":
                                    $procedure="getterTaxIncome";
                                    break;
                                case "ProductionIncome":
                                    $procedure="getterProductionIncome";
                                    break;
                                case "ManpowerRecovery":
                                    $procedure="getterManpowerRecovery";
                                    break;
                                case "SupplyLimit":
                                    $procedure="getterSupplyLimit";
                                    break;
                                }
                              $stmt = $myPDO->prepare("CALL $procedure(:id,@value)");
                      
                              // pass value to the command
                              $id=$row['ProvinceID'];
                              $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                      
                              // execute the stored procedure
                              $stmt->execute();
                              $stmt->closeCursor();

                              $rowD = $myPDO->query("SELECT @value AS Value")->fetch(PDO::FETCH_ASSOC);
                              print("<td>$rowD[Value]</td>");
                            }
                            elseif ($col=="HRE" || $col=="MOH") {
                              if ($row[$col]==0) {
                                print("<td>Not Member</td>");
                              }
                              else {
                                print("<td>Member</td>");
                              }
                            }
                            else {
                              print("<td>$row[$col]</td>");
                            }
                           
                          }
                        ?>
                      </tr>
                      <?php 
                          }
                        }
                      ?>
                      </tbody>
                      <tfoot>
                      <tr>
                        <?php 
                          foreach( $filter as $col ){
                            print("<th>$col</th>");
                          }
                          ?>
                      </tr>
                      </tfoot>
                      
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
    </section>
    <?php 
      }
    //Finish Data Table 

    //for Geographical Map
      if($_GET["type"]=="gm") {
    ?>

    <section class="content-header">
      <h1>
        Geographical
        <small>Stats</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content container-fluid">

        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Type</th>
                        <th>Terrain</th>
                        <th>Province Count</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php  
                      // datas content
                        $query = $myPDO->query("select geographicalMap.type, geographicalMap.terrain, count(provinces.name) as totalProvince from provinces
                        join geographicalMap on geographicalMap.terrainID = provinces.geographicalMap_terrainID
                          group by geographicalMap.type,geographicalMap.terrain ;");
                        if ( $query->rowCount() ){
                          foreach( $query as $row ){
                               
                      ?>
                      <tr>
                        <td><?php print($row['type']); ?></td>
                        <td><?php print($row['terrain']); ?></td>
                        <td><?php print($row['totalProvince']); ?></td>
                      </tr>
                      <?php 
                          }
                        }
                      ?>
                      </tbody>
                      <tfoot>
                      <tr>
                        <th>Type</th>
                        <th>Terrain</th>
                        <th>Province Count</th>
                      </tr>
                      </tfoot>
                      
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
    </section>
    <?php 
      }
    //Finish Data Table 
    
    //for Cultures 
    if($_GET["type"]=="c") {
      ?>
  
      <section class="content-header">
        <h1>
          Cultures
          <small>Stats</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
          <li class="active">Here</li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content container-fluid">
  
          <div class="row">
              <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Data Table With Full Features</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>Name</th>
                          <th>Province Count</th>
                          <th>Total Devolopment</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  
                        // datas content
                          $query = $myPDO->query("select cultures.name as name, floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev from provinces
                          join cultures on cultures.cultureID = provinces.cultures_cultureID
                            join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                              join statType on provinceToStat.statTypeID =statType.statTypeID
                                where property='base' 
                                group by cultures.name;");
                          if ( $query->rowCount() ){
                            foreach( $query as $row ){
                                 
                        ?>
                        <tr>
                          <td><?php print($row['name']); ?></td>
                          <td><?php print($row['totalProvince']); ?></td>
                          <td><?php print($row['totalDev']); ?></td>
                        </tr>
                        <?php 
                            }
                          }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                          <th>Name</th>
                          <th>Province Count</th>
                          <th>Total Devolopment</th>
                        </tr>
                        </tfoot>
                        
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
      </section>
      <?php 
        }
      //Finish Data Table 
      
      //for Owners
    if($_GET["type"]=="o") {
      ?>
  
      <section class="content-header">
        <h1>
          Owners
          <small>Stats</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
          <li class="active">Here</li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content container-fluid">
  
          <div class="row">
              <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Data Table With Full Features</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>Name</th>
                          <th>Province Count</th>
                          <th>Total Devolopment</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  
                        // datas content
                          $query = $myPDO->query("select owners.name as name, floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev from provinces
                          join owners on owners.ownerID = provinces.owners_ownerID
                            join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                              join statType on provinceToStat.statTypeID =statType.statTypeID
                                where property='base'
                                group by owners.name;");
                          if ( $query->rowCount() ){
                            foreach( $query as $row ){
                                 
                        ?>
                        <tr>
                          <td><?php print($row['name']); ?></td>
                          <td><?php print($row['totalProvince']); ?></td>
                          <td><?php print($row['totalDev']); ?></td>
                        </tr>
                        <?php 
                            }
                          }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                          <th>Name</th>
                          <th>Province Count</th>
                          <th>Total Devolopment</th>
                        </tr>
                        </tfoot>
                        
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
      </section>
      <?php 
        }
      //Finish Data Table 
      
      //For Religions
      if($_GET["type"]=="r") {
        ?>
    
        <section class="content-header">
          <h1>
            Religions
            <small>Stats</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
            <li class="active">Here</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
    
            <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>Name</th>
                            <th>Province Count</th>
                            <th>Total Devolopment</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php  
                          // datas content
                            $query = $myPDO->query("select religion.name , floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev from provinces
                            join religion on religion.religionID = provinces.religion_religionID
                              join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                                join statType on provinceToStat.statTypeID =statType.statTypeID
                                  where property='base'
                                  group by religion.name;");
                            if ( $query->rowCount() ){
                              foreach( $query as $row ){
                                   
                          ?>
                          <tr>
                            <td><?php print($row['name']); ?></td>
                            <td><?php print($row['totalProvince']); ?></td>
                            <td><?php print($row['totalDev']); ?></td>
                          </tr>
                          <?php 
                              }
                            }
                          ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <th>Name</th>
                            <th>Province Count</th>
                            <th>Total Devolopment</th>
                          </tr>
                          </tfoot>
                          
                        </table>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
        </section>
        <?php 
          }
        //Finish Data Table 
        
        //For Trade Nodes
      if($_GET["type"]=="tn") {
        ?>
    
        <section class="content-header">
          <h1>
            Trade Nodes
            <small>Stats</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
            <li class="active">Here</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
    
            <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>Name</th>
                            <th>Province Count</th>
                            <th>Total Devolopment</th>
                            <th>Total Produced</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php  
                          // datas content
                            $query = $myPDO->query("select tradeNodes.node as name, floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev , sum((1+pEff.value)*(1-provinces.autonomy)*(baseP.value)*(product.goodValue)) AS TotalProduced from provinces
                            join tradeNodes on tradeNodes.nodeID = provinces.tradeNodes_nodeID
                              join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                                join statType on provinceToStat.statTypeID =statType.statTypeID
                                  join product ON product.productID =provinces.product_productID
                                    join 
                                    (select provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
                                      join statType ON provinceToStat.statTypeID =statType.statTypeID
                                        where category='P' and property='Production Efficiency') as pEff
                                                      on pEff.provincesID = provinces.provincesID
                                    join ( select provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
                                        join statType on provinceToStat.statTypeID =statType.statTypeID
                                        where category='P' and property='base') as baseP
                                        on baseP.provincesID = provinces.provincesID
                                    where property = 'base'
                                  group by tradeNodes.node;");
                            if ( $query->rowCount() ){
                              foreach( $query as $row ){
                                   
                          ?>
                          <tr>
                            <td><?php print($row['name']); ?></td>
                            <td><?php print($row['totalProvince']); ?></td>
                            <td><?php print($row['totalDev']); ?></td>
                            <td><?php print($row['TotalProduced']); ?></td>
                          </tr>
                          <?php 
                              }
                            }
                          ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <th>Name</th>
                            <th>Province Count</th>
                            <th>Total Devolopment</th>
                            <th>Total Produced</th>
                          </tr>
                          </tfoot>
                          
                        </table>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
        </section>
        <?php 
          }
        //Finish Data Table 
        
        //For Products
      if($_GET["type"]=="p") {
        ?>
    
        <section class="content-header">
          <h1>
            Products
            <small>Stats</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
            <li class="active">Here</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
    
            <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>Name</th>
                            <th>Trade Category</th>
                            <th>Province Count</th>
                            <th>Total Devolopment</th>
                            <th>Total Produced</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php  
                          // datas content
                            $query = $myPDO->query("select product.name as name, tradeCategory.category as category, floor(count(provinces.name)/3) as totalProvince , sum(provinceToStat.value) as totalDev , sum((1+pEff.value)*(1-provinces.autonomy)*(baseP.value)*(product.goodValue)) AS TotalProduced from provinces
                            join product on product.productID = provinces.product_productID
                              join tradeCategory on tradeCategory.categoryID = product.tradeCategory_categoryID
                                join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                                  join statType on provinceToStat.statTypeID =statType.statTypeID
                                            join 
                                  (select provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
                                    join statType ON provinceToStat.statTypeID =statType.statTypeID
                                      where category='P' and property='Production Efficiency') as pEff
                                                    on pEff.provincesID = provinces.provincesID
                                  join ( select provinceToStat.value as value,provinceToStat.provincesID as provincesID FROM provinceToStat 
                                      join statType on provinceToStat.statTypeID =statType.statTypeID
                                      where category='P' and property='base') as baseP
                                      on baseP.provincesID = provinces.provincesID
                                  where property = 'base'
                                            group by product.name,tradeCategory.category;");
                            if ( $query->rowCount() ){
                              foreach( $query as $row ){
                                   
                          ?>
                          <tr>
                            <td><?php print($row['name']); ?></td>
                            <td><?php print($row['category']); ?></td>
                            <td><?php print($row['totalProvince']); ?></td>
                            <td><?php print($row['totalDev']); ?></td>
                            <td><?php print($row['TotalProduced']); ?></td>
                          </tr>
                          <?php 
                              }
                            }
                          ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <th>Name</th>
                            <th>Trade Category</th>
                            <th>Province Count</th>
                            <th>Total Devolopment</th>
                            <th>Total Produced</th>
                          </tr>
                          </tfoot>
                          
                        </table>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
        </section>
        <?php 
          }
        //Finish Data Table 
        //For MemberShips
      if($_GET["type"]=="m") {
        ?>
    
        <section class="content-header">
          <h1>
            Memberships
            <small>Stats</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
            <li class="active">Here</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
    
            <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>Name</th>
                            <th>Member Count</th>
                            <th>Total Development</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php  
                          // datas content
                            $query = $myPDO->query("
                            SELECT HRE.totalProvince as totalProvince1,MOH.totalProvince as totalProvince2,HRE.totalDev AS totalDev1,MOH.totalDev AS totalDev2 FROM 
                            (
                              select provinces.HRE , floor(count(provinces.name)/3) as totalProvince, sum(provinceToStat.value) as totalDev , count(provinces.provincesID)*100/((Select Count(*) From provinces)*3) as percentage from provinces
                                join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                                  join statType on provinceToStat.statTypeID =statType.statTypeID
                                    where property = 'base' and provinces.HRE=TRUE
                                      group by provinces.HRE
                            ) as HRE,
                            (
                              select provinces.MOH , floor(count(provinces.name)/3) as totalProvince, sum(provinceToStat.value) as totalDev , count(provinces.provincesID)*100/((Select Count(*) From provinces)*3) as percentage from provinces
                                join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                                  join statType on provinceToStat.statTypeID =statType.statTypeID
                                    where property = 'base' and provinces.MOH=TRUE
                                    group by provinces.MOH
                            ) as MOH;
                            ");
                            if ( $query->rowCount() ){
                              foreach( $query as $row ){
                                   
                          ?>
                          <tr>
                            <td>Holy Roman Empire</td>
                            <td><?php print($row['totalProvince1']); ?></td>
                            <td><?php print($row['totalDev1']); ?></td>
                          </tr>
                          <tr>
                            <td>Mandate of Heaven</td>
                            <td><?php print($row['totalProvince2']); ?></td>
                            <td><?php print($row['totalDev2']); ?></td>
                          </tr>
                          
                          <?php 
                              }
                            }
                          ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <th>Name</th>
                            <th>Province Count</th>
                            <th>Total Devolopment</th>
                            <th>Total Produced</th>
                          </tr>
                          </tfoot>
                          
                        </table>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
        </section>
        <?php 
          }
        //Finish Data Table 
        
        //For CONTINENTS
      if($_GET["type"]=="pmc") {
        ?>
    
        <section class="content-header">
          <h1>
            Continent
            <small>Stats</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
            <li class="active">Here</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
    
            <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>Continent</th>
                            <th>Total Super Region</th>
                            <th>Total Region</th>
                            <th>Total Area</th>
                            <th>Total Province</th>
                            <th>Total Development</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php  
                          // datas content
                            $query = $myPDO->query("
                            select geoPoliticMap.Continent as Continent , count(distinct geoPoliticMap.SuperRegion) as TotalSuperRegion , count(distinct geoPoliticMap.region) as TotalRegion , count(distinct geoPoliticMap.area) as TotalArea , count(distinct geoPoliticMap.ProvinceName) as TotalProvince, sum(provinceDev.development) as TotalDev from  geoPoliticMap
                                left join
                            (select sum(provinceToStat.value) as development , provinces.provincesID as ID  from provinces
                                join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                                  join statType on statType.statTypeID = provinceToStat.statTypeID
                                    where property = 'base' 
                                            group by provinces.provincesID) as provinceDev on provinceDev.ID = geoPoliticMap.ProvinceID 
                                            group by geoPoliticMap.Continent ;
                            ");
                            if ( $query->rowCount() ){
                              foreach( $query as $row ){
                                   
                          ?>
                          <tr>
                            <td><?php print($row['Continent']); ?></td>
                            <td><?php print($row['TotalSuperRegion']); ?></td>
                            <td><?php print($row['TotalRegion']); ?></td>
                            <td><?php print($row['TotalArea']); ?></td>
                            <td><?php print($row['TotalProvince']); ?></td>
                            <td><?php print($row['TotalDev']); ?></td>
                          </tr>
                          
                          <?php 
                              }
                            }
                          ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <th>Continent</th>
                            <th>Total Super Region</th>
                            <th>Total Region</th>
                            <th>Total Area</th>
                            <th>Total Province</th>
                            <th>Total Development</th>
                          </tr>
                          </tfoot>
                          
                        </table>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
        </section>
        <?php 
          }
        //Finish Data Table 
        
        //SuperRegion
        if($_GET["type"]=="pmsr") {
          ?>
      
          <section class="content-header">
            <h1>
              Super Regions
              <small>Stats</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
              <li class="active">Here</li>
            </ol>
          </section>
          <!-- Main content -->
          <section class="content container-fluid">
      
              <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>Continent</th>
                              <th>Super Region</th>
                              <th>Total Region</th>
                              <th>Total Area</th>
                              <th>Total Province</th>
                              <th>Total Development</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php  
                            // datas content
                              $query = $myPDO->query("
                              select geoPoliticMap.Continent as Continent,  geoPoliticMap.SuperRegion  as SuperRegion , count(distinct geoPoliticMap.region) as TotalRegion , count(distinct geoPoliticMap.area) as TotalArea , count(distinct geoPoliticMap.ProvinceName) as TotalProvince, sum(provinceDev.development) as TotalDev from  geoPoliticMap
                                  left join
                              (select sum(provinceToStat.value) as development , provinces.provincesID as ID  from provinces
                                  join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                                    join statType on statType.statTypeID = provinceToStat.statTypeID
                                      where property = 'base' 
                                              group by provinces.provincesID) as provinceDev on provinceDev.ID = geoPoliticMap.ProvinceID 
                                              group by geoPoliticMap.SuperRegion,geoPoliticMap.Continent ;
                              ");
                              if ( $query->rowCount() ){
                                foreach( $query as $row ){
                                     
                            ?>
                            <tr>
                              <td><?php print($row['Continent']); ?></td>
                              <td><?php print($row['SuperRegion']); ?></td>
                              <td><?php print($row['TotalRegion']); ?></td>
                              <td><?php print($row['TotalArea']); ?></td>
                              <td><?php print($row['TotalProvince']); ?></td>
                              <td><?php print($row['TotalDev']); ?></td>
                            </tr>
                            
                            <?php 
                                }
                              }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                              <th>Continent</th>
                              <th>Super Region</th>
                              <th>Total Region</th>
                              <th>Total Area</th>
                              <th>Total Province</th>
                              <th>Total Development</th>
                            </tr>
                            </tfoot>
                            
                          </table>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
          </section>
          <?php 
            }
          //Finish Data Table 
          

          //SuperRegion
        if($_GET["type"]=="pmr") {
          ?>
      
          <section class="content-header">
            <h1>
              Super Regions
              <small>Stats</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
              <li class="active">Here</li>
            </ol>
          </section>
          <!-- Main content -->
          <section class="content container-fluid">
      
              <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>Continent</th>
                              <th>Super Region</th>
                              <th>Region</th>
                              <th>Total Area</th>
                              <th>Total Province</th>
                              <th>Total Development</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php  
                            // datas content
                              $query = $myPDO->query("
                              select geoPoliticMap.Continent as Continent,  geoPoliticMap.SuperRegion  as SuperRegion,  geoPoliticMap.region  as Region, count(distinct geoPoliticMap.area) as TotalArea , count(distinct geoPoliticMap.ProvinceName) as TotalProvince, sum(provinceDev.development) as TotalDev from  geoPoliticMap
                                  left join
                              (select sum(provinceToStat.value) as development , provinces.provincesID as ID  from provinces
                                  join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                                    join statType on statType.statTypeID = provinceToStat.statTypeID
                                      where property = 'base' 
                                              group by provinces.provincesID) as provinceDev on provinceDev.ID = geoPoliticMap.ProvinceID 
                                              group by geoPoliticMap.Region,geoPoliticMap.SuperRegion,geoPoliticMap.Continent ;
                              ");
                              if ( $query->rowCount() ){
                                foreach( $query as $row ){
                                     
                            ?>
                            <tr>
                              <td><?php print($row['Continent']); ?></td>
                              <td><?php print($row['SuperRegion']); ?></td>
                              <td><?php print($row['Region']); ?></td>
                              <td><?php print($row['TotalArea']); ?></td>
                              <td><?php print($row['TotalProvince']); ?></td>
                              <td><?php print($row['TotalDev']); ?></td>
                            </tr>
                            
                            <?php 
                                }
                              }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                              <th>Continent</th>
                              <th>Super Region</th>
                              <th>Region</th>
                              <th>Total Area</th>
                              <th>Total Province</th>
                              <th>Total Development</th>
                            </tr>
                            </tfoot>
                            
                          </table>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
          </section>
          <?php 
            }
          //Finish Data Table 
          
          //Areas
        if($_GET["type"]=="pma") {
          ?>
      
          <section class="content-header">
            <h1>
              Super Regions
              <small>Stats</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
              <li class="active">Here</li>
            </ol>
          </section>
          <!-- Main content -->
          <section class="content container-fluid">
      
              <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>Continent</th>
                              <th>Super Region</th>
                              <th>Region</th>
                              <th>Area</th>
                              <th>Total Province</th>
                              <th>Total Development</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php  
                            // datas content
                              $query = $myPDO->query("
                              select geoPoliticMap.Continent as Continent,  geoPoliticMap.SuperRegion  as SuperRegion,  geoPoliticMap.region  as Region, geoPoliticMap.area AS Area ,count(distinct geoPoliticMap.ProvinceName) as TotalProvince, sum(provinceDev.development) as TotalDev from  geoPoliticMap
                                  left join
                              (select sum(provinceToStat.value) as development , provinces.provincesID as ID  from provinces
                                  join provinceToStat on provinceToStat.provincesID = provinces.provincesID
                                    join statType on statType.statTypeID = provinceToStat.statTypeID
                                      where property = 'base'
                                              group by provinces.provincesID) as provinceDev on provinceDev.ID = geoPoliticMap.ProvinceID 
                                              group by geoPoliticMap.Area,geoPoliticMap.Region,geoPoliticMap.SuperRegion,geoPoliticMap.Continent ;
                              ");
                              if ( $query->rowCount() ){
                                foreach( $query as $row ){
                                     
                            ?>
                            <tr>
                              <td><?php print($row['Continent']); ?></td>
                              <td><?php print($row['SuperRegion']); ?></td>
                              <td><?php print($row['Region']); ?></td>
                              <td><?php print($row['Area']); ?></td>
                              <td><?php print($row['TotalProvince']); ?></td>
                              <td><?php print($row['TotalDev']); ?></td>
                            </tr>
                            
                            <?php 
                                }
                              }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                              <th>Continent</th>
                              <th>Super Region</th>
                              <th>Region</th>
                              <th>Area</th>
                              <th>Total Province</th>
                              <th>Total Development</th>
                            </tr>
                            </tfoot>
                            
                          </table>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
          </section>
          <?php 
            }
          //Finish Data Table ?>



    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Kadir Karayakalı - Berkcan Erguncu
    </div>
    <!-- Default to the left -->
    <strong>Statistics of Europa Universalis IV in 1444 <a href="https://eu4.paradoxwikis.com/Europa_Universalis_4_Wiki" target="_blank">Reference</a>.</strong>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>