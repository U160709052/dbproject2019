<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Europa Universalis 4 DB Project</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="dist/css/skins/skin-green.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-green sidebar-mini">
<?php
    try {
        $myPDO = new PDO('mysql:host=localhost:3306;dbname=euIVdb;charset=utf8', 'root', '385914');
        //$link = mysqli_connect("localhost:3306", "root", "385914", "euIVdb");
    } 
    catch ( PDOException $e ){
        print $e->getMessage();
    }
?>
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>EU</b>IV</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>EU-IV</b> DB</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">CATEGORIES OF DATABASE</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="index.php?type=ap"><i class="fa fa-database"> </i> <span>All Provinces</span></a></li>
        <li><a href="index.php?type=gm"><i class="fa fa-globe"></i> <span>Geographical Map</span></a></li>
        <li class="treeview">
          <a href="#"><i class="fa fa-map"></i> <span>Political Map</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?type=pmc">Continent</a></li>
            <li><a href="index.php?type=pmsr">Super Regions</a></li>
            <li><a href="index.php?type=pmr">Regions</a></li>
            <li><a href="index.php?type=pma">Areas</a></li>
          </ul>
        </li>
        <li><a href="index.php?type=c"><i class="fa fa-user"></i> <span>Cultures</span></a></li>
        <li><a href="index.php?type=o"><i class="fa fa-flag"></i> <span>Owners</span></a></li>
        <li><a href="index.php?type=r"><i class="fa fa-heart"></i></i> <span>Religions</span></a></li>
        <li><a href="index.php?type=tn"><i class="fa fa-dollar"></i> <span>Trade Nodes</span></a></li>
        <li><a href="index.php?type=p"><i class="fa fa-industry"></i> <span>Product</span></a></li>
        <li><a href="index.php?type=m"><i class="fa fa-empire"></i> <span>Memberships</span></a></li>
        <li><a href="charts.php"><i class="fa fa-pie-chart"></i> <span>Charts</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        CHARTS
        <small>Percentage</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php?type=ap"><i class="fa fa-dashboard"></i> Main Page</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <div class="row">
            <div class="col-md-6">
              <!-- DONUT CHART -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Religions</h3>
    
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <canvas id="religions" style="height:250px"></canvas>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
    
            </div>
            <!-- /.col (LEFT) -->
            <div class="col-md-6">
              <!-- DONUT CHART -->
              <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title">Product</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <canvas id="product" style="height:250px"></canvas>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col (RIGHT) -->
          </div>
          <!-- /.row -->
          <div class="row">
            <div class="col-md-6">
              <!-- DONUT CHART -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Holy Roman Empire</h3>
    
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <canvas id="hre" style="height:250px"></canvas>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
    
            </div>
            <!-- /.col (LEFT) -->
            <div class="col-md-6">
              <!-- DONUT CHART -->
              <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title">Mandate of Heaven</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <canvas id="moh" style="height:250px"></canvas>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col (RIGHT) -->
          </div>
          <!-- /.row -->
        
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Kadir Karayakalı - Berkcan Erguncu
    </div>
    <!-- Default to the left -->
    <strong>Statistics of Europa Universalis IV in 1444 <a href="https://eu4.paradoxwikis.com/Europa_Universalis_4_Wiki" target="_blank">Reference</a>.</strong>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- page script -->
<?php
function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}
print(random_color())
?>

<script>
    $(function () {
      /* ChartJS
       * -------
       * Here we will create a few charts using ChartJS
       */
      //-------------
      //- PIE CHART -
      //-------------
      // Get context with jQuery - using jQuery's .get() method.
      
      var pieChartCanvas = $('#religions').get(0).getContext('2d')
      var pieChart       = new Chart(pieChartCanvas)
      var PieData        = [
        <?php
        $query = $myPDO->query("select religion.name as name,count(provinces.provincesID)*100/(Select Count(*) From geoPoliticMap where type='Land') as percentage from provinces JOIN religion ON provinces.religion_religionID=religion.religionID
        group by religion.name;");
        if ( $query->rowCount() ){
          foreach( $query as $row ){
        ?>
        {
          
          value    : <?php print($row['percentage']); ?>,
          color    : '#<?php print(random_color()); ?>',
          highlight: '#<?php print(random_color()); ?>',
          label    : '<?php print($row['name']); ?>'
        },
        <?php
          }
        }
        ?>
      ]
      var pieOptions     = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
      }
      //Create pie or douhnut chart
      // You can switch between pie and douhnut using the method below.
      pieChart.Doughnut(PieData, pieOptions)



      var pieChartCanvas = $('#product').get(0).getContext('2d')
      var pieChart       = new Chart(pieChartCanvas)
      var PieData        = [
        <?php
        $query = $myPDO->query("select product.name as name,count(provinces.provincesID)*100/(Select Count(*) From geoPoliticMap where type='Land') as percentage from provinces JOIN product ON provinces.product_productID=product.productID
        group by product.name;");
        $total=0;
        if ( $query->rowCount() ){
          foreach( $query as $row ){
            $total=$total+$row['percentage'];
        ?>
        {
          
          value    : <?php print($row['percentage']); ?>,
          color    : '#<?php print(random_color()); ?>',
          highlight: '#<?php print(random_color()); ?>',
          label    : '<?php print($row['name']); ?>'
        },
        <?php
          }
        }
        ?>
        {
          
          value    : <?php print(100-$total); ?>,
          color    : '#<?php print(random_color()); ?>',
          highlight: '#<?php print(random_color()); ?>',
          label    : 'Uncolonized'
        }
      ]
      var pieOptions     = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
      }
      //Create pie or douhnut chart
      // You can switch between pie and douhnut using the method below.
      pieChart.Doughnut(PieData, pieOptions)

      var pieChartCanvas = $('#hre').get(0).getContext('2d')
      var pieChart       = new Chart(pieChartCanvas)
      var PieData        = [
        <?php
        $query = $myPDO->query("select count(provinces.provincesID)*100/(Select Count(*) From geoPoliticMap where type='Land') as percentage from provinces where HRE=1;");
        $total=0;
        if ( $query->rowCount() ){
          foreach( $query as $row ){
            $total=$total+$row['percentage']
        ?>
        {
          
          value    : <?php print($row['percentage']); ?>,
          color    : '#<?php print(random_color()); ?>',
          highlight: '#<?php print(random_color()); ?>',
          label    : 'Member'
        },
        <?php
          }
        }
        ?>
        {
          
          value    : <?php print(100-$total); ?>,
          color    : '#<?php print(random_color()); ?>',
          highlight: '#<?php print(random_color()); ?>',
          label    : 'Not Member'
        }
      ]
      var pieOptions     = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
      }
      //Create pie or douhnut chart
      // You can switch between pie and douhnut using the method below.
      pieChart.Doughnut(PieData, pieOptions)


      var pieChartCanvas = $('#moh').get(0).getContext('2d')
      var pieChart       = new Chart(pieChartCanvas)
      var PieData        = [
        <?php
        $query = $myPDO->query("select count(provinces.provincesID)*100/(Select Count(*) From geoPoliticMap where type='Land') as percentage from provinces where MOH=1;");
        $total=0;
        if ( $query->rowCount() ){
          foreach( $query as $row ){
            $total=$total+$row['percentage']
        ?>
        {
          
          value    : <?php print($row['percentage']); ?>,
          color    : '#<?php print(random_color()); ?>',
          highlight: '#<?php print(random_color()); ?>',
          label    : 'Member'
        },
        <?php
          }
        }
        ?>
        {
          
          value    : <?php print(100-$total); ?>,
          color    : '#<?php print(random_color()); ?>',
          highlight: '#<?php print(random_color()); ?>',
          label    : 'Not Member'
        }
      ]
      var pieOptions     = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
      }
      //Create pie or douhnut chart
      // You can switch between pie and douhnut using the method below.
      pieChart.Doughnut(PieData, pieOptions)

    })
  </script>
</body>
</html>